EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:led_tester-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LED_RCGB The_Led1
U 1 1 59F9221B
P 3100 1750
F 0 "The_Led1" H 3100 2120 50  0000 C CNN
F 1 "LED_RCGB" H 3100 1400 50  0000 C CNN
F 2 "LEDshield:LED-RGB-5050-COMMON_CATHODE_MINIMAL" H 3100 1700 50  0001 C CNN
F 3 "" H 3100 1700 50  0001 C CNN
	1    3100 1750
	-1   0    0    1   
$EndComp
$Comp
L LED_RCGB The_Led2
U 1 1 59F917DA
P 3100 2600
F 0 "The_Led2" H 3100 2970 50  0000 C CNN
F 1 "LED_RCGB" H 3100 2250 50  0000 C CNN
F 2 "LEDshield:LED-RGB-5050-COMMON_CATHODE_MINIMAL" H 3100 2550 50  0001 C CNN
F 3 "" H 3100 2550 50  0001 C CNN
	1    3100 2600
	-1   0    0    1   
$EndComp
$Comp
L LED_RCGB The_Led3
U 1 1 59F91ADB
P 3100 3450
F 0 "The_Led3" H 3100 3820 50  0000 C CNN
F 1 "LED_RCGB" H 3100 3100 50  0000 C CNN
F 2 "LEDshield:LED-RGB-5050-COMMON_CATHODE_MINIMAL" H 3100 3400 50  0001 C CNN
F 3 "" H 3100 3400 50  0001 C CNN
	1    3100 3450
	-1   0    0    1   
$EndComp
$Comp
L LED_RCGB The_Led4
U 1 1 59F91AEB
P 3100 4300
F 0 "The_Led4" H 3100 4670 50  0000 C CNN
F 1 "LED_RCGB" H 3100 3950 50  0000 C CNN
F 2 "LEDshield:LED-RGB-5050-COMMON_CATHODE_MINIMAL" H 3100 4250 50  0001 C CNN
F 3 "" H 3100 4250 50  0001 C CNN
	1    3100 4300
	-1   0    0    1   
$EndComp
Wire Wire Line
	3300 1750 3500 1750
Wire Wire Line
	2900 1550 2650 1550
Wire Wire Line
	3300 2600 3500 2600
Wire Wire Line
	3300 3450 3500 3450
Wire Wire Line
	3500 4300 3300 4300
Wire Wire Line
	2650 4100 2900 4100
Wire Wire Line
	2650 1200 2650 4100
Connection ~ 2650 1550
Wire Wire Line
	2900 3250 2650 3250
Connection ~ 2650 3250
Wire Wire Line
	2900 2400 2650 2400
Connection ~ 2650 2400
Wire Wire Line
	2400 4300 2900 4300
Wire Wire Line
	2400 1000 2400 4300
Wire Wire Line
	2150 1000 2150 4500
Wire Wire Line
	2150 4500 2900 4500
Wire Wire Line
	2900 3450 2400 3450
Connection ~ 2400 3450
Wire Wire Line
	2900 2600 2400 2600
Connection ~ 2400 2600
Connection ~ 2400 1750
Wire Wire Line
	2900 1950 2150 1950
Connection ~ 2150 1950
Wire Wire Line
	2900 2800 2150 2800
Connection ~ 2150 2800
Wire Wire Line
	2900 3650 2150 3650
Connection ~ 2150 3650
Wire Wire Line
	2900 1750 2400 1750
Wire Wire Line
	3500 1750 3500 4300
Connection ~ 3500 2600
Connection ~ 3500 3450
Wire Wire Line
	3500 3050 4350 3050
Connection ~ 3500 3050
Wire Wire Line
	4350 3050 4350 1300
Wire Wire Line
	4350 1300 2900 1300
Wire Wire Line
	2900 1300 2900 1000
$Comp
L Conn_01x04 J1
U 1 1 59F9C32A
P 2400 800
F 0 "J1" H 2400 1000 50  0000 C CNN
F 1 "Conn_01x04" H 2400 500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm_SMD_Pin1Left" H 2400 800 50  0001 C CNN
F 3 "" H 2400 800 50  0001 C CNN
	1    2400 800 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2150 1000 2300 1000
Wire Wire Line
	2650 1200 2500 1200
Wire Wire Line
	2500 1200 2500 1000
Wire Wire Line
	2900 1000 2600 1000
$EndSCHEMATC
